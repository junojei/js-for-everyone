import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  // todo: show fighter info (image, name, health, etc.)

  if (!fighter) {
    return fighterElement;
  }

  const fighterInfoCard = createElement({
    tagName: 'div',
    className: 'fighter-preview___card'
  });

  const fighterImage = createFighterImage(fighter);
  const fighterInfo = createElement({
    tagName: 'div',
    className: 'fighter-preview___info'
  });

  for (const [key, value] of Object.entries(fighter)) {
    if (key === '_id' || key === 'source') {
      continue
    }

    let fighterInfoElement = createElement({
      tagName: 'p',
      className: `fighter-preview___info-${key}`
    });

    fighterInfoElement.innerHTML = `${key}: ${value}`;

    fighterInfo.append(fighterInfoElement);
  }

  fighterInfoCard.append(fighterImage);
  fighterInfoCard.append(fighterInfo);

  fighterElement.append(fighterInfoCard)

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}