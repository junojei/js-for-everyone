import { showModal } from './modal';
import { createElement } from '../../helpers/domHelper';

export function showWinnerModal(fighter) {
  // call showModal function 

  const modalBody = createElement({
    tagName: 'div',
    className: 'modal-body'
  });
  modalBody.innerHTML = '';

  showModal({
    title: `${fighter.name} wins`,
    bodyElement: modalBody
  });
}
