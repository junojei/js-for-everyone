import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over

    let keysPressed = new Set();
    let isFirstCooldown = false;
    let isSecondCooldown = false;

    const firstFighterHealthIndicator = document.getElementById("left-fighter-indicator");
    const secondFighterHealthIndicator = document.getElementById("right-fighter-indicator");

    const healthStore = [firstFighter.health, secondFighter.health];

    const keyDownListener = (e) => {
      keysPressed.add(e.code);

      let damage = 0;

      if ((e.code == controls.PlayerOneAttack) && !keysPressed.has(controls.PlayerOneBlock)) {
        if (!keysPressed.has(controls.PlayerTwoBlock)) {
          damage = getDamage(firstFighter, secondFighter);
        } else {
          damage = getHitPower(firstFighter);
        }
        secondFighter.health -= damage;
      }

      if ((e.code == controls.PlayerTwoAttack) && !keysPressed.has(controls.PlayerTwoBlock)) {
        if (!keysPressed.has(controls.PlayerOneBlock)) {
          damage = getDamage(secondFighter, firstFighter);
        } else {
          damage = getHitPower(secondFighter);
        }
        firstFighter.health -= damage;
      }

      if (controls.PlayerOneCriticalHitCombination.every((k) => keysPressed.has(k))) {
        if (!isFirstCooldown) {
          damage = getCriticalHit(firstFighter);
          isFirstCooldown = true;
          setTimeout(() => isFirstCooldown = false, 10000);
          secondFighter.health -= damage;
        }
      }

      if (controls.PlayerTwoCriticalHitCombination.every((k) => keysPressed.has(k))) {
        if (!isSecondCooldown) {
          damage = getCriticalHit(secondFighter);
          isSecondCooldown = true;
          setTimeout(() => isSecondCooldown = false, 10000);
          firstFighter.health -= damage;
        }
      }


      firstFighterHealthIndicator.style.width = `${Math.round((firstFighter.health / healthStore[0]) * 100)}%`;
      secondFighterHealthIndicator.style.width = `${Math.round((secondFighter.health / healthStore[1]) * 100)}%`;

      if (firstFighter.health <= 0) {
        removeListeners();
        resolve(secondFighter);
      }

      if (secondFighter.health <= 0) {
        removeListeners();
        resolve(firstFighter);
      }
    };

    const keyUpListener = (e) => {
      keysPressed.delete(e.code);
    };

    document.body.addEventListener('keydown', keyDownListener);
    document.body.addEventListener('keyup', keyUpListener);

    const removeListeners = () => {
      document.body.removeEventListener('keydown', keyDownListener);
      document.body.removeEventListener('keyup', keyUpListener);
    };
  });
}

export function getDamage(attacker, defender) {
  // return damage

  const damage = getHitPower(attacker) - getBlockPower(defender);
  return (damage <= 0) ? 0 : damage;
}

export function getHitPower(fighter) {
  // return hit power

  const criticalHitChance = Math.random() + 1;
  return fighter.attack * criticalHitChance;
}

export function getBlockPower(fighter) {
  // return block power

  const dodgeChance = Math.random() + 1;
  return fighter.defense * dodgeChance;
}

export function getCriticalHit(fighter) {
  // return block power

  return fighter.attack * 2;
}